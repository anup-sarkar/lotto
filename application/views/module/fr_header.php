<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE HTML>
 <html>
  <head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Lotto Admin Panel</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="71Bangladesh" />
  <meta name="keywords" content="71Bangladesh" />
  <meta name="author" content="71Bangladesh" />

    <!-- Facebook and Twitter integration -->
  <meta property="og:title" content=""/>
  <meta property="og:image" content=""/>
  <meta property="og:url" content=""/>
  <meta property="og:site_name" content=""/>
  <meta property="og:description" content=""/>
  <meta name="twitter:title" content="" />
  <meta name="twitter:image" content="" />
  <meta name="twitter:url" content="" />
  <meta name="twitter:card" content="" />

  <!-- <link href="https://fonts.googleapis.com/css?family=Droid+Sans" rel="stylesheet"> -->

  <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/admin_styles.css">
  <!-- Animate.css -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<!------ Include the above in your HEAD tag ---------->
  <!-- FOR IE9 below -->
  <!--[if lt IE 9]>
  <script src="js/respond.min.js"></script>
  <![endif]-->

  </head>
  <body>

    <div id="wrapper">
    <nav class="navbar header-top fixed-top navbar-expand-lg ">
      <a class="navbar-brand" href="#" style="color:black">71<b>BD</b> Franchisee</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText"
        aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarText">
        <ul class="navbar-nav side-nav">
          <li class="nav-item <?php if($flag=="home") echo "active"; ?>"">
            <a class="nav-link" href="<?php echo site_url('Franchisee/index'); ?>" ><i class="fa fa-tachometer" aria-hidden="true"></i> &nbsp;Dashboard
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item <?php if($flag=="pro") echo "active"; ?>"">
            <a class="nav-link" href="<?php echo site_url('Franchisee/profile'); ?>" ><i class="fa fa-user-circle-o" aria-hidden="true"></i>&nbsp; View Profile</a>
          </li>

         
          <li class="nav-item <?php if($flag=="form") echo "active"; ?>"">
            <a class="nav-link" href="<?php echo site_url('franchisee/completed_forms'); ?>" ><i class="fa fa-file-text" aria-hidden="true"></i> &nbsp;Work Form</a>
          </li>
        </ul>


        <ul class="navbar-nav ml-md-auto d-md-flex">
          <li class="nav-item">
            <a class="nav-link" href="#"><i class="fa fa-user-o" aria-hidden="true"></i>  <?php  echo $_SESSION['client_name' ]; ?>
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo site_url('admin/logout'); ?>" ><i class="fa fa-sign-out" aria-hidden="true"></i> Sign Out </a>
          </li>
           
        </ul>
      </div>
    </nav>
    
 
    
   