<?php

/**
 * @property  email
 */
class Ticket_model extends CI_Model
{
    function __construct()
    {
        
        parent::__construct();
    }
    
    
    
    function insert($data)
    {
        return $this->db->insert('items', $data);
    }
    
     
    
    
    public function get()
    {
        
       
        
        
        $this->db->select('items.id, items.Date,items.ShopID,items.Ticket, group_concat(items.Value SEPARATOR ",") as Value,shop.Shop_name,sum(items.Value) as total')
        ->from('items')
        ->join('shop', 'items.ShopID = shop.id')
        ->group_by('items.Date,items.ShopID,items.Ticket');
        $query = $this->db->get();
        
        
        return $query->result();
    }
    
    
    public function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('items');
        if($this->db->affected_rows() > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    
    
    
 
    
    
}