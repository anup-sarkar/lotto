<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_Stock extends CI_Migration {
    
    public function up()
    {
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'Date' => array(
                'type' => 'Date'
                
            ),
            
            
            'ShopID' => array(
                'type' => 'INT',
                'constraint' => 5,
   
            ) 
            
            ,
            '_30_Stock' => array(
                'type' => 'INT',
                'constraint' => 5,
                
            )
            ,
            '_20_Stock' => array(
                'type' => 'INT',
                'constraint' => 5,
                
            )
            ,
            '_10_Stock' => array(
                'type' => 'INT',
                'constraint' => 5,
                
            )
            ,
            '_5_Stock' => array(
                'type' => 'INT',
                'constraint' => 5,
                
            )
            ,
            '_2_Stock' => array(
                'type' => 'INT',
                'constraint' => 5,
                
            ) ,
            '_1_Stock' => array(
                'type' => 'INT',
                'constraint' => 5,
                
            )
            
            
            , 'Updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP',
        ));
        $this->dbforge->add_key('id', TRUE);
        
        $this->dbforge->create_table('Stock');
        
    }
    
    public function down()
    {
        $this->dbforge->drop_table('Stock');
    }
}