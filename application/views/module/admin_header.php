<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE HTML>
 <html>
  <head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>LOTO Admin Panel</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="Loto" />
  <meta name="keywords" content="Loto" />
  <meta name="author" content="Loto" />

    <!-- Facebook and Twitter integration -->
  <meta property="og:title" content=""/>
  <meta property="og:image" content=""/>
  <meta property="og:url" content=""/>
  <meta property="og:site_name" content=""/>
  <meta property="og:description" content=""/>
  <meta name="twitter:title" content="" />
  <meta name="twitter:image" content="" />
  <meta name="twitter:url" content="" />
  <meta name="twitter:card" content="" />

 <link href="https://fonts.googleapis.com/css?family=Droid+Sans" rel="stylesheet">

  <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/admin_styles.css">
  <!-- Animate.css -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
 
  </head>
  <body>


    <div id="wrapper">
    <nav class="navbar header-top fixed-top navbar-expand-lg " style="background-color:#28d4ef">
      <a class="navbar-brand" href="#" style="color: black;font-size:20px"><i class="fa fa-rocket" aria-hidden="true"></i> <b>LOTO </b> ADMIN PANEL</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText"
        aria-expanded="false" aria-label="Toggle navigation">
      <i class="fa fa-align-justify" aria-hidden="true"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarText">
        <ul class="navbar-nav side-nav" style="background-color: #212529">
          <li class="nav-item <?php if($flag=="home") echo "active"; ?>">
            <a class="nav-link" href="<?php echo site_url('admin/index'); ?>" ><i class="fa fa-tachometer" aria-hidden="true"></i> &nbsp;Dashboard
              <span class="sr-only">(current)</span>
            </a>
          </li>

          <li class="nav-item <?php if($flag=="file") echo "active"; ?>">
            <a class="nav-link" href="<?php echo site_url('admin/events'); ?>" ><i class="fa fa-university" aria-hidden="true"></i> &nbsp;Shops</a>
          </li>

   <li class="nav-item <?php if($flag=="acc_c") echo "active"; ?>">
            <a class="nav-link" href="<?php echo site_url('admin/create'); ?>" ><i class="fa fa-user-plus" aria-hidden="true"></i>&nbsp;Setup </a>
          </li>
   
   
     		 <li class="nav-item <?php if($flag=="acc_c") echo "active"; ?>">
            <a class="nav-link" href="<?php echo site_url('admin/create'); ?>" ><i class="fa fa-file-text" aria-hidden="true"></i>&nbsp;Reports </a>
          </li>
   

          <li class="nav-item <?php if($flag=="acc") echo "active"; ?>">
            <a class="nav-link" href="<?php echo site_url('admin/accounts'); ?>" ><i class="fa fa-ticket" aria-hidden="true"></i>&nbsp; Tickets</a>
          </li>

      	 <li class="nav-item <?php if($flag=="acc") echo "active"; ?>">
            <a class="nav-link" href="<?php echo site_url('admin/accounts'); ?>" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp; Todays Entry</a>
          </li>
          
          <li class="nav-item <?php if($flag=="acc") echo "active"; ?>">
            <a class="nav-link" href="<?php echo site_url('admin/accounts'); ?>" ><i class="fa fa-calculator" aria-hidden="true"></i>&nbsp; Accounting </a>
          </li>
           
          
          
        </ul>


        <ul class="navbar-nav ml-md-auto d-md-flex">
          <li class="nav-item">
            <a class="nav-link" href="#"><i class="fa fa-user-o" aria-hidden="true"></i> Super Admin
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo site_url('admin/logout'); ?>" ><i class="fa fa-sign-out" aria-hidden="true"></i> Sign Out </a>
          </li>
           
        </ul>
      </div>
    </nav>
    
    <style>
    .nav-link
    {
    color:grey;
    }
    </style>
 
    
   