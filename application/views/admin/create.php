<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$dat['flag']='acc_c';
$this->load->view("module/admin_header",$dat );
?>

   
<div class="container" style="width:50%;text-align: center;">
    <div class="card card-container" style="padding:5%">
        <!-- <img class="profile-img-card" src="//lh3.googleusercontent.com/-6V8xOA6M7BA/AAAAAAAAAAI/AAAAAAAAAAA/rzlHcD0KYwo/photo.jpg?sz=120" alt="" /> -->
        <img id="profile-img" class="profile-img-card" style="width: 120px;margin:0 auto" src="<?php echo base_url(); ?>assets/images/profile.png" />
        <h3 id="profile-name" class="profile-name-card">Register Form</h3>
        <br/>

  <?php $attributes = array("class" => "", "id" => "registerForm", "name" => "registerForm");
          echo form_open("admin/register", $attributes);?>

            <div class="form-group">
                                                    <input type="text" placeholder="First Name" id="fname" name="fname" class="form-control" value="<?php echo set_value('fname'); ?>">

                                                    <span class="text-danger"><?php echo form_error('fname'); ?></span>
                                                </div>


                                                <div class="form-group">
                                                    <input type="text" placeholder="Last Name" id="lname" name="lname" class="form-control" value="<?php echo set_value('lname'); ?>">

                                                      <span class="text-danger"><?php echo form_error('lname'); ?></span>
                                                </div>


                                                <div class="form-group">
                                                    <input type="number" placeholder="Mobile No." id="mobile" name="mobile" class="form-control" value="<?php echo set_value('mobile'); ?>">

                                                      <span class="text-danger"><?php echo form_error('mobile'); ?></span>
                                                </div>

                                                <div class="form-group">
                                                    <input type="email" placeholder="Enter email" id="email" name="email"  class="form-control" value="<?php echo set_value('email'); ?>">

                                                      <span class="text-danger"><?php echo form_error('email'); ?></span>
                                                </div>


                                                <div class="form-group">
                                                    <input type="password" placeholder="Password"  id="pass" name="pass"class="form-control" value="<?php echo set_value('pass'); ?>">

                                                      <span class="text-danger"><?php echo form_error('pass'); ?></span>
                                                </div>

                                                           <div class="form-group">
                                                  <select class="form-control" name="type">
                                                   
                                                    <option value="Super User">Super User</option>
                                                  </select>
                                                </div>
                                            
                                            


            <button class="btn btn-primary btn-block btn-signin" type="submit"><i class="fa fa-user-circle-o" aria-hidden="true"></i> Register</button>
           
             <?php echo form_close(); ?>
                             
                             <br/>
                             <?php echo $this->session->flashdata('msg'); ?>               
    </div><!-- /card-container -->
</div><!-- /container -->
 




<br/>
 
 
 </div>
  </body>
</html>






 