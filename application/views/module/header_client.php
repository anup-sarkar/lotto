<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE HTML>
 <html>
  <head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>71Bangladesh</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="71Bangladesh" />
  <meta name="keywords" content="71Bangladesh" />
  <meta name="author" content="71Bangladesh" />

    <!-- Facebook and Twitter integration -->
  <meta property="og:title" content=""/>
  <meta property="og:image" content=""/>
  <meta property="og:url" content=""/>
  <meta property="og:site_name" content=""/>
  <meta property="og:description" content=""/>
  <meta name="twitter:title" content="" />
  <meta name="twitter:image" content="" />
  <meta name="twitter:url" content="" />
  <meta name="twitter:card" content="" />

  <!-- <link href="https://fonts.googleapis.com/css?family=Droid+Sans" rel="stylesheet"> -->


  <!-- Animate.css -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/animate.css">
  <!-- Icomoon Icon Fonts-->
  <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/icomoon.css">
  <!-- Themify Icons-->
  <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/themify-icons.css">
  <!-- Bootstrap  -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/bootstrap.css">

  <!-- Magnific Popup -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/magnific-popup.css">

  <!-- Owl Carousel  -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/owl.carousel.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/owl.theme.default.min.css">

  <!-- Theme style  -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/style.css">

  <!-- Modernizr JS -->
  <script src="<?php echo base_url(); ?>/assets/js/modernizr-2.6.2.min.js"></script>
  <!-- FOR IE9 below -->
  <!--[if lt IE 9]>
  <script src="js/respond.min.js"></script>
  <![endif]-->

  </head>
  <body>