<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_shop extends CI_Migration {

        public function up()
        {
                $this->dbforge->add_field(array(
                        'id' => array(
                                'type' => 'INT',
                                'constraint' => 5,
                                'unsigned' => TRUE,
                                'auto_increment' => TRUE
                        ),
                        'Shop_name' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '100',
                        ),
  
                      
                        'Address' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '100',
                        ),
 
                        'Mobile' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '15',
                        ),
                        'Email' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '100',
                        ),
                         'Pass' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '100',
                        ) 
                        ,'Status' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '1',
                                'default' => '1'
                        )
                         ,'Type' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '20',
                                'default' => 'Normal User'
                        )
                        , 'Updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP',
                ));
                $this->dbforge->add_key('id', TRUE);
                 
                $this->dbforge->create_table('shop');

        }

        public function down()
        {
                $this->dbforge->drop_table('shop');
        }
}