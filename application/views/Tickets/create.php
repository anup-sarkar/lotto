<?php
defined('BASEPATH') or exit('No direct script access allowed');
$dat['flag'] = 'acc_c';
$this->load->view("module/admin_header", $dat);
?>


<div class="container" style="width: 50%; text-align: center;">
	<div class="card card-container" style="padding: 5%">
		<!-- <img class="profile-img-card" src="//lh3.googleusercontent.com/-6V8xOA6M7BA/AAAAAAAAAAI/AAAAAAAAAAA/rzlHcD0KYwo/photo.jpg?sz=120" alt="" /> -->
		<img id="profile-img" class="profile-img-card"
			style="width: 120px; margin: 0 auto"
			src="<?php echo base_url(); ?>assets/images/ticket.png" /> <br />
		<h2 id="profile-name" class="profile-name-card" style="color: #009df0">ADD
			VALUE</h2>
		<br />

  <?php

$attributes = array(
    "class" => "",
    "id" => "registerForm",
    "name" => "registerForm"
);
echo form_open("Tickets/create_item", $attributes);
?>


											<div class="form-group">
			<select class="form-control" name="ShopID">
                                                   
                                                       
                                                     <?php
                                                    foreach ($Shops as $item) {
                                                        echo '<option value="' . $item->id . '">' . $item->Shop_name . ' </option>';
                                                    }

                                                    ?>
                                                  </select>
		</div>


		<div class="form-group">
			<input type="date" required id="Date" name="Date"
				class="form-control" value="<?php echo set_value('Date'); ?>"> <span
				class="text-danger"><?php echo form_error('Date'); ?></span>
		</div>




		<div class="form-group">
			<select class="form-control" name="Ticket">

				<option value="30">$30</option>
				<option value="20">$20</option>
				<option value="10">$10</option>
				<option value="5">$5</option>
				<option value="2">$2</option>
				<option value="1">$1</option>
			</select>
		</div>



		<div class="form-group">
			<input type="number" required placeholder="Value" id="Value"
				name="Value" class="form-control"
				value="<?php echo set_value('Value'); ?>"> 
				
				<span class="text-danger"><?php echo form_error('Value'); ?></span>
		</div>

		<div id="Add">
		
		</div>


 
			 		<input type="hidden" class="form-control" name="TotalVal" value="" id="TotalVal" />
		 
		
		
		<input type="hidden" name="Total" value="1" id="Total" />
		 



		<a class="btn btn-default btn-block btn-signin" style="border:1px solid" id="AddMore">
			<i class="fa fa-ticket" aria-hidden="true"></i> Add More Value
		</a>
		
		
		<button class="btn btn-primary btn-block btn-signin" type="submit">
			<i class="fa fa-plus-circle" aria-hidden="true"></i> Submit
		</button>
		
		  <button type="button" style="display: none" class="btn btn-info btn-lg" id="Modal" data-toggle="modal" data-target="#myModal">Open Modal</button>
		
		
		
           
             <?php echo form_close(); ?>
                             
                             <br />
                             <?php echo $this->session->flashdata('msg'); ?>               
    </div>
	<!-- /card-container -->
</div>
<!-- /container -->





<br />


</div>
<div class="container">
   
  <!-- Trigger the modal with a button -->

  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
        
          <h3 class="modal-title" ><i class="fa fa-ticket"></i> Add More Value</h3>
        </div>
        <div class="modal-body">
          <input type="number"   placeholder="More Value" id="MoreValue"
				name="MoreValue" class="form-control"
				 />
        </div>
        <div class="modal-footer">
         <button type="button" class="btn btn-primary" id="MoreSubmits"   ><i class="fa fa-plus-circle" aria-hidden="true"></i> ADD</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  
</div>

<script type="text/javascript">

var i=1;
		$("#AddMore").click(function(){

			$("#Modal").click(); 
			
			$("#MoreValue").focus(); 
			});


		$("#MoreSubmits").click(function(){
				var v=$("#MoreValue").val();
				var v2=parseInt(v);
				 console.log(v);

				 if(v == "" || v2 <= 0 )
				 {

					 $("#MoreValue").focus();
					 alert("Please Enter Valid Value!");
				 }
				 else
				 {
					 

						var element="<div class='form-group'><input type='number' readonly='readonly'  id='More'"+
						"  class='form-control'"+
							"value='"+ v +"'></div>";

					 
					 $('#myModal').modal('hide');
					 $("#MoreValue").val(0);

					 $("#Add").append(element);

					 i++;
					$("#Total").val(i);

					if(i==2)
					{
						$("#TotalVal").val(v);
								
					}
					else

					{
						var totalVal=$("#TotalVal").val();
						totalVal=totalVal +","+v;
						$("#TotalVal").val(totalVal);
					}
					
					
				 }
				
				 
		});


		 $("#Value").change(function(){

			 var v=this.value;
			  
			 if(i==1)
				{
					$("#TotalVal").val(v);
							
				}
				else

				{
					var totalVal=$("#TotalVal").val();
					totalVal=totalVal +","+v;
					$("#TotalVal").val(totalVal);
				}
			    
			  });

		  
</script>



</body>
</html>






