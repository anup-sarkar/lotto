<?php

class Tickets extends CI_Controller
{
    
    function __construct()
    {
        parent::__construct();
    }
    
    public function index()
    {
        $this->isLogin();
        
        $data['Items'] = $this->Ticket_model->get();
        
        
        
        $this->load->view('tickets/index',$data);
        
    }
    
    
    public function create()
    {
        $this->isLogin();
        
        $data['Shops'] = $this->Shop_model->get();
        $this->load->view('tickets/create' ,$data);
        
    }
    
    
    
    
    
    public function isLogin()
    {
        
        if (!isset($_SESSION['client_name'])  ) {
            
            
            redirect("admin/login");
        }
        
    }
    
    public function delete($id)
    {
        $this->isLogin();
        if ($this->Ticket_model->delete($id)) {
            $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Item has been Deleted !</div>');
            redirect('tickets/index');
        } else {
            $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Account Deletion Failed!</div>');
            redirect('tickets/index');
        }
        
        // echo var_dump($data);
    }
    
    
    
    
    
    public function create_item()
    {
        $this->isLogin();
        // set validation rules
        $this->form_validation->set_rules('ShopID', 'Shop Name', 'trim|required|numeric');
        $this->form_validation->set_rules('Date', 'Date', 'trim|required');
        $this->form_validation->set_rules('Ticket', 'Ticket', 'trim|required');
        
        $this->form_validation->set_rules('Value', 'Value', 'trim|required');
        
     
        // $this->form_validation->set_rules('type', 'Account Type', 'required|min_length[1]');
        
        // validate form input
        if ($this->form_validation->run() == FALSE) {
            // fails
            
            $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Errors : Please Recheck Following Problems ! </div>');
            $this->load->view('Tickets/create');
            // $this->load->view('account/register');
        } else {
            // insert the user registration details into database
            
            $total= $this->input->post('Total');
            $totalVal= $this->input->post('TotalVal');
            $totalArray=explode(",",$totalVal);
            
            $total=(int)$total;
            
            $error=false;
            
            $data = array(
                'ShopID' => $this->input->post('ShopID'),
                'Date' => $this->input->post('Date'),
                'Ticket' => $this->input->post('Ticket'),
                'Value' =>  $this->input->post('Value')
                
                
            );
            
            
            if (!$this->Ticket_model->insert($data)) {
                $error=true;
                
            }  
            
            
            
            for($i=0;$i<$total-1;$i++)
            {
                  
                
               $data = array(
                   'ShopID' => $this->input->post('ShopID'),
                   'Date' => $this->input->post('Date'),
                   'Ticket' => $this->input->post('Ticket'),
                   'Value' => (int)$totalArray[$i]
                   
                   
               );
               
               
               if (!$this->Ticket_model->insert($data)) {
                   $error=true;
                  
               }  
            }
            
            
        
            if ($error) {
                $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Oops! Error.  Please try again later!!!</div>');
                redirect('Ticket/create');
                
             
            } else {
                 
          
                $this->session->set_flashdata('msg', '<div class="alert alert-success text-center">Items Successfully Registered. </div>');
                redirect('Tickets/index');
            }
             
           
        }
    }
    
    
}