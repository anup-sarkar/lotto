<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Keith Meredith</title>
    <meta name="description" content="Keith Meredith">
    <meta name="keywords" content="Keith Meredith">
    
    <link href="https://fonts.googleapis.com/css?family=Inconsolata|Rubik:300,400,700,900" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/keith/css/styles-merged.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/keith/css/style.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/keith/css/custom.css">

    <!--[if lt IE 9]>
      <script src="js/vendor/html5shiv.min.js"></script>
      <script src="js/vendor/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

  <!-- START: header -->

  <header role="banner" class="probootstrap-header">
    <div class="container-fluid">
        <a href="<?php echo site_url('site/index'); ?>" class="probootstrap-logo">Keith Meredith<span>.</span></a>
        
        <a href="#" class="probootstrap-burger-menu visible-xs" ><i>Menu</i></a>
        <div class="mobile-menu-overlay"></div>

        <nav role="navigation" class="probootstrap-nav hidden-xs">
          <ul class="probootstrap-main-nav">
            <li class="active"><a href="index.html">Home</a></li>
            <li><a href="#">Carrier Links </a></li>
            <li><a href="#">Stay Informed </a></li>
            <li><a href="#">About Me</a></li>
            <li><a href="#">Contact</a></li>

          </ul>
          <ul class="probootstrap-header-social hidden-xs">
            <li><a href="#"><i class="icon-twitter"></i></a></li>
            <li><a href="#"><i class="icon-facebook2"></i></a></li>
            <li><a href="#"><i class="icon-instagram2"></i></a></li>
          </ul>
          <div class="extra-text visible-xs">
            <a href="#" class="probootstrap-burger-menu"><i>Menu</i></a>
            <h5>Connect With Me</h5>
            <ul class="social-buttons">
              <li><a href="#"><i class="icon-twitter"></i></a></li>
              <li><a href="#"><i class="icon-facebook2"></i></a></li>
              <li><a href="#"><i class="icon-instagram2"></i></a></li>
            </ul>
          </div>
        </nav>
    </div>
  </header>
  <!-- END: header -->
  
  <!-- START: section -->
  <section class="probootstrap-intro probootstrap-intro-inner" style="background-image: url(img/hero_bg_1_b.jpg);" data-stellar-background-ratio="0.5">
    <div class="container">
      <div class="row">
        <div class="col-md-7 probootstrap-intro-text">
          <h1 class="probootstrap-animate" data-animate-effect="fadeIn">Events</h1>
          <div class="probootstrap-subtitle probootstrap-animate" data-animate-effect="fadeIn">
            <h2>Check Out All My <a href="">Upcoming Events</a></h2>
          </div>
        </div>
      </div>
    </div>
    <span class="probootstrap-animate"><a class="probootstrap-scroll-down js-next" href="#next-section">Scroll down <i class="icon-chevron-down"></i></a></span>
  </section>
  <!-- END: section -->
  <main>
    <section id="next-section" class="probootstrap-section">
      <div class="container">
        <div class="row">
          <div class="col-md-12 probootstrap-relative">
            <h2 class="probootstrap-heading mt0 mb50"> All Events </h2>
            <ul class="probootstrap-owl-navigation absolute right">
              <li><a href="#" class="probootstrap-owl-prev"><i class="icon-chevron-left"></i></a></li>
              <li><a href="#" class="probootstrap-owl-next"><i class="icon-chevron-right"></i></a></li>
            </ul>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 probootstrap-relative">
            <div class="owl-carousel owl-carousel-carousel">
  <?php 
 foreach ($Events as $item)
            {
 
              ?>


              <div class="item">
                <div class="probootstrap-program">
                  <a href="#"><img src="<?php echo $item->Path; ?>" alt="<?php echo $item->Name; ?>" class="img-responsive img-rounded"></a>
                  <h3><?php echo $item->Name; ?></h3>
                  <h4><i class="fa fa-map-marker"></i> <?php echo $item->Location; ?></h4>
                  <p><i class="fa fa-calendar"> </i>  <?php echo $item->DateStart; ?>-<?php echo $item->DateEnd; ?></p>
                  <p><?php echo $item->Details; ?></p>
                </div>
              </div>


                <?php 
}
 
              ?>
             
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
      </main>
   <footer class="probootstrap-footer" style="background-color: #31708f">
      <div class="probootstrap-backtotop"><a href="#" class="js-backtotop"><i class="icon-chevron-thin-up"></i></a></div>
      <div class="container">
      <div class="row mb50">
        <div class="col-md-3">
          <div class="probootstrap-footer-widget">
            <h4>About Keith Meredith</h4>
            <p>A Little details about Keith Meredith <a href="#">Learn More</a></p>
          </div>
        </div>
        <div class="col-md-3 col-md-push-1">
          <div class="probootstrap-footer-widget">
            <h4>Upcoming Events</h4>
            <ul class="probootstrap-footer-link">
              <li>Week Days: 05:00 – 22:00</li>
              <li>Saturday: CLOSED</li>
              <li>Sunday: 05:00 - 22:00</li>
            </ul>
          </div>
        </div>

        <div class="col-md-5 col-md-push-1">
          <div class="probootstrap-footer-widget">
            <h4>Links</h4>
            <ul class="probootstrap-footer-link float">
                 <li><a href="<?php echo site_url('admin/index'); ?>" class=" ">Admin Login</a></li>
              <li><a href="#">Home</a></li>
              <li><a href="#">Programs</a></li>
              <li><a href="#">Gallery</a></li>
            </ul>
            <ul class="probootstrap-footer-link float">
              <li><a href="#">About Me</a></li>
              <li><a href="#">Contact</a></li>
              <li><a href="#">Privacy</a></li>
            </ul>
            <ul class="probootstrap-footer-link float">
              <li><a href="#">About Me</a></li>
              <li><a href="#">Join The Club</a></li>
              <li><a href="#">Pricing</a></li>
            </ul>
          </div>
          <div class="probootstrap-footer-widget">
            <h4>Contact Info</h4>
            <ul class="probootstrap-contact-info">
              <li><i class="icon-location2"></i> <span>198 West 21th Street, Suite 721 New York NY 10016</span></li>
              <li><i class="icon-mail"></i><span>info@domain.com</span></li>
              <li><i class="icon-phone2"></i><span>+123 456 7890</span></li>
            </ul>
          </div>
        </div>
        
      </div>
       <div class="row" style="display: none;">
         <div class="col-md-12 text-center border-top">
           <p class="mb0">&copy; Copyright 2017 <a href="https://uicookies.com/">uiCookies:Trainer</a>. All Rights Reserved. <br> Designed by <a href="https://uicookies.com">uicookies.com</a> Demo Images Unsplash.com &amp; Pexels.com</p>
         </div>
       </div>
     </div>
  </footer> 

  <style type="text/css">
    a
    {
       text-shadow: 2px 2px 1px #000000;
    }

     .no-shadow
    {
       text-shadow: 0px 0px 0px #000000;
    }
  </style> 

<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <script src="<?php echo base_url(); ?>/assets/keith/js/scripts.min.js"></script>
  <script src="<?php echo base_url(); ?>/assets/keith/js/main.min.js"></script>
  <script src="<?php echo base_url(); ?>/assets/keith/js/custom.js"></script>

  </body>
</html>