<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$dat['flag']='acc';
$this->load->view("module/admin_header",$dat );
?>

      <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card" style="padding: 20px">
                            <div class="header">
                                <h2 class="title">All Accounts <a class="btn btn-primary" 
                                href="<?php echo base_url().'index.php/admin/create' ?>" 
                                style="float: right"> Add New Account </a> </h2>
                               
                              
                            </div>
                            <div class="content table-responsive table-full-width">





                                <table class="table table-hover"  >
                                    <thead>
                                        <tr><th>SL</th>
                                      <th>Name</th>
                                  
                                       <th>Mobile</th>
                                      <th>Email</th>
                                      
                                        <th>Type</th>
                                  
                                    
                                       <th>Status</th>
                                  
                                      <th></th>
                                    </tr></thead>
                                    <tbody>

<?php

$i=1;
            foreach ($Users as $item)
            {
                  $url=base_url();
                  $id=$item->id; 
                  $name=$item->Fname." ".$item->Lname; 
                  $text="";
                  if($item->Status==1)
                  {
                    $text='<p style="color:green" >Active </p>';
                  }
                  else
                  {
                    $text='<p style="color:red" >Inactive </p>';
                  }



                                       echo "<tr>";
                                       echo "<td>".$i."</td>"; 
                                      
                                       echo "<td>".$name."</td>";   
                                  
                                          echo "<td>".$item->Mobile."</td>";
                                           echo "<td>".$item->Email."</td>";
                                           echo "<td>".$item->Type."</td>";
                                         
                                             echo "<td>".$text."</td>";
                                  

                                  
                                

                                       echo "<td> <a class='btn btn-danger' href='".$url."index.php/admin/delete/".$id."' ><i class='fa fa-times' aria-hidden='true'></i>   </a>  </td>
                                      ";
                                       echo    "</tr>";
                                       $i++; 

            }
                                        ?>

                                        <tr> <?php echo $this->session->flashdata('msg'); ?>  </tr>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>


                 


                </div>
            </div>
        </div>

  


<br/>
 
 
 </div>
  </body>
</html>

