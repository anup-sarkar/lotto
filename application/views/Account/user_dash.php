<?php
   defined('BASEPATH') OR exit('No direct script access allowed');
   
    
   ?>


<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Hello Hi Parking</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url(); ?>assets/mobile/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="<?php echo base_url(); ?>assets/mobile/vendor/fontawesome-free/css/all.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/mobile/vendor/simple-line-icons/css/simple-line-icons.css">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">
 <meta name="theme-color" content="#000000">
<link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
    <!-- Custom styles for this template -->
  <link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url(); ?>assets/img/favicon/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url(); ?>assets/img/favicon/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url(); ?>assets/img/favicon/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url(); ?>assets/img/favicon/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url(); ?>assets/img/favicon/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url(); ?>assets/img/favicon/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url(); ?>assets/img/favicon/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url(); ?>assets/img/favicon/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url(); ?>assets/img/favicon/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo base_url(); ?>assets/img/favicon/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url(); ?>assets/img/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url(); ?>assets/img/favicon/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url(); ?>assets/img/favicon/favicon-16x16.png">
<link rel="manifest" href="<?php echo base_url(); ?>assets/img/favicon/manifest.json">
<meta name="msapplication-TileColor" content="#000000">
<meta name="msapplication-TileImage" content="<?php echo base_url(); ?>assets/img/favicon/ms-icon-144x144.png">


    <!-- Plugin CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/mobile/device-mockups/device-mockups.min.css">

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url(); ?>assets/mobile/css/new-age.min.css" rel="stylesheet">

  </head>

  <body id="page-top" style="overflow: hidden;position:fixed;background: url(<?php echo base_url(); ?>assets/images/bg2.png);background-repeat: no-repeat;    background-size: auto;    background-size: contain, cover;">
 
    <!-- Navigation -->
    

    <header class="masthead" style="padding-top: 100px;background: none">
      <div class="container h-100">
        <div class="row h-100">
           
          <div class="col-lg-5 my-auto">
            <div class="device-container">
              <div class="device-mockup iphone6_plus portrait white">
                <div class="centered" style="text-align: center;">
                  <img class="img-responsive" style="" src="<?php echo base_url(); ?>assets/images/team.png">
                  <a class="btn_gradient" href="<?php echo site_url('home/login_mob'); ?>"   style="width: 100%" >  Login / Sign Up </a>
                </div>

                


                 <div class="centered" style="text-align: center;">
                  <img class="img-responsive" style="" src="<?php echo base_url(); ?>assets/images/profile.png">
                  <a class="btn_gradient" href="<?php echo site_url('home/profile'); ?>"   style="width: 100%" ><i class="fa fa-ticket"></i> Profile </a>
                </div>


              </div>
            </div>
          </div>
        </div>
      </div>
    </header>

   <div id="preloader">



  <div id="status">&nbsp;
  
  </div>
  
</div>
 

 

     <script src="<?php echo base_url(); ?>assets/mobile/vendor/jquery/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/mobile/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url(); ?>assets/mobile/vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for this template -->
    <script src="<?php echo base_url(); ?>assets/mobile/js/new-age.min.js"></script>
 

    <!-- Bootstrap core JavaScript -->
 
  </body>

  <style type="text/css">
    *::-webkit-input-placeholder {
    color: white;
}
*:-moz-placeholder {
    /* FF 4-18 */
    color: white;
    opacity: 1;
}
*::-moz-placeholder {
    /* FF 19+ */
    color: white;
    opacity: 1;
}
*:-ms-input-placeholder {
    /* IE 10+ */
    color: white;
}
*::-ms-input-placeholder {
    /* Microsoft Edge */
    color: white;
}
*::placeholder {
    /* modern browser */
    color: white;
}

 
  .img-responsive {
  
  width: 40%;
  vertical-align: top;
}
.centered
{
  padding: 40px;padding-bottom: 0px;text-align: center;
  padding-top: 30px;
}
a:hover {
 text-decoration:none;
}

.btn_gradient{

cursor: pointer;
transition: .5s ease;
color:white;
background: linear-gradient(40deg,#45cafc,#303f9f)!important;
border-radius: 10em;
box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
margin: .375rem;
border: 0;
text-transform: uppercase;
white-space: normal;
word-wrap: break-word;
font-weight: 400;
display: inline-block;
text-align: center;

font-family: Lato,Helvetica,Arial,sans-serif;
letter-spacing: 2px;
user-select: none;
align-items: flex-start;
padding: 13px;

  
}
  </style>




</html>
