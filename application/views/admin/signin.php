<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$dat['flag']='acc_c';
$this->load->view("module/admin_header",$dat );
?>

</div>
 <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/signin.css">
 
  <div class="text-center" style="margin: 0 auto">

  <?php $attributes = array("class" => "form-signin", "id" => "registerForm", "name" => "registerForm");
          echo form_open("admin/login_check", $attributes);?>

<img class="mb-4" src="<?php echo base_url(); ?>assets/images/logo2.png" alt="" >
      <h1 class="h3 mb-3 font-weight-bold" style="color:#0cb0d3"  >LOTO</b> Admin  Login</h1>
      
      <label for="inputEmail" class="sr-only" >Email address</label>
      <input type="email" name="l_email" placeholder="Email" class="form-control" value="<?php echo set_value('l_email'); ?>" required autofocus>
       <span class="text-danger"><?php echo form_error('l_email'); ?></span>
                                        
                                        
      <label for="inputPassword" class="sr-only">Password</label>
      <input type="password"   class="form-control" placeholder="Password" id="l_pass" name="l_pass" class="form-control" value="<?php echo set_value('l_pass'); ?>"  required>
        <span class="text-danger"><?php echo form_error('l_pass'); ?></span>
                                   
      <div class="checkbox mb-3">
        <label>
          <input type="checkbox" value="remember-me"> Remember me
        </label>
      </div>
      <button class="btn btn-lg btn-success btn-block" type="submit" style="background:#0cb0d3"><i class="fa fa-user-circle"></i> LOGIN</button>
          <?php echo $this->session->flashdata('msg'); ?>  
     
      <p class="mt-5 mb-3 text-muted">All Right Reserved   &copy; 2018-2019</p>

     <?php echo form_close(); ?>
     
     </div>

<style>
<!--
#wrapper
{
display:none;
}
-->
</style>

 


 
 
 
  
  </body>
</html>


