<?php

class Shops extends CI_Controller
{
    
    function __construct()
    {
        parent::__construct();
    }
    
    public function index()
    {
        $this->isLogin();
        
        $data['Shops'] = $this->Shop_model->get();
        $this->load->view('shop/index',$data);
        
    }
    
    
    public function create()
    {
        $this->isLogin();
        
        
        $this->load->view('shop/create' );
        
    }
    
    
    
    
    
    public function isLogin()
    {
        
        if (!isset($_SESSION['client_name'])  ) {
            
            
            redirect("admin/login");
        }
        
    }
    
    public function delete($id)
    {
        $this->isLogin();
        if ($this->Shop_model->delete($id)) {
            $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Shop has been Deleted !</div>');
            redirect('shops/index');
        } else {
            $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Shop Deletion Failed!</div>');
            redirect('shops/index');
        }
        
        // echo var_dump($data);
    }
    
    
    
    
    
    public function create_shop()
    {
        $this->isLogin();
        // set validation rules
        $this->form_validation->set_rules('sname', 'Shop Name', 'trim|required|min_length[3]|max_length[100] ');
        $this->form_validation->set_rules('addr', 'Address', 'trim|required|min_length[3]|max_length[100] ');
        $this->form_validation->set_rules('email', 'Email ID', 'trim|required|valid_email');
        
        $this->form_validation->set_rules('mobile', 'Mobile Number', 'trim|required|min_length[9]|max_length[14] ');
        
        $this->form_validation->set_rules('pass', 'Password', 'trim|required|md5|min_length[4]|max_length[40]');
        // $this->form_validation->set_rules('type', 'Account Type', 'required|min_length[1]');
        
        // validate form input
        if ($this->form_validation->run() == FALSE) {
            // fails
            
            $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Errors : Please Recheck Following Problems ! </div>');
            $this->load->view('shop/create');
            // $this->load->view('account/register');
        } else {
            // insert the user registration details into database
            $data = array(
                'Shop_name' => $this->input->post('sname'),
                'Address' => $this->input->post('addr'),
                'Email' => $this->input->post('email'),
                'Mobile' => $this->input->post('mobile'),
                'Pass' => $this->input->post('pass'),
                'Type' => $this->input->post('type')
            );
            
            // insert form data into database
            if ($this->Shop_model->insert($data)) {
                $this->session->set_flashdata('msg', '<div class="alert alert-success text-center">Shop Successfully Registered. </div>');
                redirect('shops/index');
            } else {
                // echo "Failed";
                // error
                $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Oops! Error.  Please try again later!!!</div>');
                redirect('shops/create');
            }
        }
    }
    
    
}