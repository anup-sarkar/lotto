<?php

/**
 * @property  email
 */
class Shop_model extends CI_Model
{
    function __construct()
    {
        
        parent::__construct();
    }
    
    
    
    function insert($data)
    {
        return $this->db->insert('shop', $data);
    }
    
    
    
    public function get()
    {
        
        $this->db->select('*');
        $this->db->from('shop');  
      
        
        
        
        $sql= $this->db->get();
        
        
        $result=$sql->result();
        
        return $result;
    }
    
    
    
    public function get_user($id)
    {
        
        $this->db->select('*');
        $this->db->from('shop');  
        $this->db->where('id', $id);
        
        
        
        $sql= $this->db->get();
        
        
        $result=$sql->result_array();
        
        return $result;
    }
    
    
    public function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('shop');
        if($this->db->affected_rows() > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    
    
}