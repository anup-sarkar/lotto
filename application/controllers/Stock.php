<?php

class Stock extends CI_Controller
{
    
    function __construct()
    {
        parent::__construct();
    }
    
    public function index()
    {
        $this->isLogin();
        
        $data['Stock'] = $this->Stock_model->get();
        
        
        
        $this->load->view('Stock/index',$data);
        
    }
    
    
    public function create($id)
    {
        $this->isLogin();
        
        if(empty($id))
        {
            $data['Stock'] = null;
        }
        else {
            $data['Stock'] = $this->Stock_model->getStockArray($id);
        }
      
        
        $data['Shop'] = $this->Shop_model->get();
        
        $this->load->view('Stock/create' ,$data);
        
    }
    
    
    
    
    
    public function isLogin()
    {
        
        if (!isset($_SESSION['client_name'])  ) {
            
            
            redirect("admin/login");
        }
        
    }
    
    public function delete($id)
    {
        $this->isLogin();
        if ($this->Stock_model->delete($id)) {
            $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Item has been Deleted !</div>');
            redirect('Stock/index');
        } else {
            $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Account Deletion Failed!</div>');
            redirect('Stock/index');
        }
        
        // echo var_dump($data);
    }
    
    
    
    
    
    public function create_stock()
    {
        $this->isLogin();
        // set validation rules
        $this->form_validation->set_rules('ShopID', 'Shop Name', 'trim|required|numeric');
        $this->form_validation->set_rules('Date', 'Date', 'trim|required');
       
        
        $this->form_validation->set_rules('v30', 'Ticket $30', 'trim|required|numeric');
        $this->form_validation->set_rules('v20', 'Ticket $20', 'trim|required|numeric');
        $this->form_validation->set_rules('v10', 'Ticket $10', 'trim|required|numeric');
        $this->form_validation->set_rules('v5', 'Ticket $5', 'trim|required|numeric');
        $this->form_validation->set_rules('v2', 'Ticket $2', 'trim|required|numeric');
        $this->form_validation->set_rules('v1', 'Ticket $1', 'trim|required|numeric');
        
        
        
        // $this->form_validation->set_rules('type', 'Account Type', 'required|min_length[1]');
        
        // validate form input
        if ($this->form_validation->run() == FALSE) {
            // fails
            
            $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Errors : Please Recheck Following Problems ! </div>');
            $this->load->view('Stock/create');
            // $this->load->view('account/register');
        } else {
            // insert the user registration details into database
            
            
            
            $data = array(
                'ShopID' => $this->input->post('ShopID'),
                'Date' => $this->input->post('Date'),
                
                '_30_Stock' =>  $this->input->post('v30'),
                '_20_Stock' =>  $this->input->post('v20'),
                '_10_Stock' =>  $this->input->post('v10'),
                '_5_Stock' =>  $this->input->post('v5'),
                '_2_Stock' =>  $this->input->post('v2'),
                '_1_Stock' =>  $this->input->post('v1'),
                
                
            );
            
            $date= $this->input->post('Date');
            $sid=$this->input->post('ShopID');
            
            if (!$this->Stock_model->insert($data,$date,$sid)) {
                $error=true;
                
            }
            
            
            
            
            
            
            
            if ($error) {
                $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Oops! Error.  Please try again later!!!</div>');
                redirect('Stock/create');
                
                
            } else {
                
                
                $this->session->set_flashdata('msg', '<div class="alert alert-success text-center">Items Successfully Registered. </div>');
                redirect('Stock/index');
            }
            
            
        }
    }
    
    
}