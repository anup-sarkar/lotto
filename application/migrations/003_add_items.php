<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_Items extends CI_Migration {
    
    public function up()
    {
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'Date' => array(
                'type' => 'Date'
                
            ),
            
            
            'ShopID' => array(
                'type' => 'INT',
                'constraint' => 5,
   
            ),
            
            'Ticket' => array(
                'type' => 'VARCHAR',
                'constraint' => '15',
            ) 
            
            ,
            'Value' => array(
                'type' => 'INT',
                'constraint' => 5,
                
            )
             
            , 'Updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP',
        ));
        $this->dbforge->add_key('id', TRUE);
        
        $this->dbforge->create_table('Items');
        
    }
    
    public function down()
    {
        $this->dbforge->drop_table('Items');
    }
}