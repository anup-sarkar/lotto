<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_Users extends CI_Migration {

        public function up()
        {
                $this->dbforge->add_field(array(
                        'id' => array(
                                'type' => 'INT',
                                'constraint' => 5,
                                'unsigned' => TRUE,
                                'auto_increment' => TRUE
                        ),
                        'Fname' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '100',
                        ),
                        'Lname' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '100',
                        )
                        ,
                        'FatherName' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '100',
                        ),

                        'DOB' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '10',
                        ),

                        'Sex' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '10',
                        ),
                        'Address' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '100',
                        ),
                        'NID' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '20',
                        ),
                        'SIS' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '50',
                        ),
                        'Slot' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '100',
                        ),
                        'IDCreationDate' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '10',
                        ),
 
                        'Mobile' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '15',
                        ),
                        'Email' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '100',
                        ),
                         'Pass' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '100',
                        ) 
                        ,'Status' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '1',
                                'default' => '1'
                        )
                         ,'Type' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '20',
                                'default' => 'Normal User'
                        )
                        , 'Updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP',
                ));
                $this->dbforge->add_key('id', TRUE);
                 
                $this->dbforge->create_table('Users');

        }

        public function down()
        {
                $this->dbforge->drop_table('Users');
        }
}