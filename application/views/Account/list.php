
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
$this->load->view("module/header_client" );
?>



      <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card" style="padding: 20px">
                            <div class="header">
                                <h2 class="title">All Accounts <a class="btn btn-primary" 
                                href="<?php echo base_url().'index.php/account/signup' ?>" 
                                style="float: right"> Add New Account </a> </h2>
                               
                              
                            </div>
                            <div class="content table-responsive table-full-width">





                                <table class="table table-hover" style="font-size:12px">
                                    <thead>
                                        <tr><th>SL</th>
                                      <th>Name</th>
                                  
                                       <th>Mobile</th>
                                      <th>Email</th>
                                  
                                    
                                       <th>Status</th>
                                  
                                      <th></th>
                                    </tr></thead>
                                    <tbody>

<?php

$i=1;
            foreach ($Users as $item)
            {
                  $url=base_url();
                  $id=$item->id; 
                  $name=$item->fname." ".$item->lname; 
                  $text="";
                  if($item->status==1)
                  {
                    $text='<p style="color:green" >Active </p>';
                  }
                  else
                  {
                    $text='<p style="color:red" >Inactive </p>';
                  }



                                       echo "<tr>";
                                       echo "<td>".$i."</td>"; 
                                      
                                       echo "<td>".$name."</td>";   
                                  
                                          echo "<td>".$item->mobile."</td>";
                                           echo "<td>".$item->email."</td>";
                                         
                                             echo "<td>".$text."</td>";
                                  

                                  
                                

                                           echo  " 
                                      <td> <a class='btn btn-danger' href='".$url."index.php/account/delete/".$id."' ><i class='fa fa-times' aria-hidden='true'></i>   </a>  </td>
                                      ";
                                       echo    "</tr>";
                                       $i++; 

            }
                                        ?>

                                        <tr> <?php echo $this->session->flashdata('msg'); ?>  </tr>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>


                 


                </div>
            </div>
        </div>



 


<?php

$this->load->view("module/footer_client");
?>
