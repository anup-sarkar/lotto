-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 15, 2019 at 04:57 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `lotto`
--

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE IF NOT EXISTS `items` (
`id` int(5) unsigned NOT NULL,
  `Date` date NOT NULL,
  `ShopID` int(5) NOT NULL,
  `Ticket` varchar(15) NOT NULL,
  `Value` int(5) NOT NULL,
  `Updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `Date`, `ShopID`, `Ticket`, `Value`, `Updated_at`) VALUES
(2, '2019-02-18', 2, '30', 30, '2019-02-14 13:54:45'),
(3, '2019-02-18', 2, '30', 60, '2019-02-14 13:54:45'),
(4, '2019-02-18', 2, '30', 90, '2019-02-14 13:54:45'),
(5, '2019-02-11', 3, '20', 55, '2019-02-14 18:28:14'),
(6, '2019-02-11', 3, '20', 10, '2019-02-14 18:28:14'),
(7, '2019-02-11', 3, '20', 99, '2019-02-14 18:28:14'),
(8, '2019-02-11', 3, '5', 66, '2019-02-14 18:28:54'),
(9, '2019-02-11', 3, '5', 566, '2019-02-14 18:30:44'),
(10, '2019-02-15', 3, '10', 10, '2019-02-14 18:31:56'),
(11, '2019-02-15', 3, '10', 20, '2019-02-14 18:31:56'),
(12, '2019-02-15', 3, '10', 30, '2019-02-14 18:31:56'),
(13, '2019-02-15', 3, '10', 40, '2019-02-14 18:31:56'),
(14, '2019-02-15', 3, '10', 50, '2019-02-14 18:31:56');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `version` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`version`) VALUES
(5);

-- --------------------------------------------------------

--
-- Table structure for table `shop`
--

CREATE TABLE IF NOT EXISTS `shop` (
`id` int(5) unsigned NOT NULL,
  `Shop_name` varchar(100) NOT NULL,
  `Address` varchar(100) NOT NULL,
  `Mobile` varchar(15) NOT NULL,
  `Email` varchar(100) NOT NULL,
  `Pass` varchar(100) NOT NULL,
  `Status` varchar(1) NOT NULL DEFAULT '1',
  `Type` varchar(20) NOT NULL DEFAULT 'Normal User',
  `Updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `shop`
--

INSERT INTO `shop` (`id`, `Shop_name`, `Address`, `Mobile`, `Email`, `Pass`, `Status`, `Type`, `Updated_at`) VALUES
(2, 'Hello Shop', 'Darus-salam, mirpur', '1676667712', 'shop@admin.com', '5d218e95e749e45c3fd7fb00d0ad9886', '1', '2', '2019-02-14 12:50:11'),
(3, 'Test SHop', 'Darus-salam, mirpur', '1676667712', 'shop2@admin.com', '0e7517141fb53f21ee439b355b5a1d0a', '1', '2', '2019-02-14 18:27:00');

-- --------------------------------------------------------

--
-- Table structure for table `stock`
--

CREATE TABLE IF NOT EXISTS `stock` (
`id` int(5) unsigned NOT NULL,
  `Date` date NOT NULL,
  `ShopID` int(5) NOT NULL,
  `_30_Stock` int(5) NOT NULL,
  `_20_Stock` int(5) NOT NULL,
  `_10_Stock` int(5) NOT NULL,
  `_5_Stock` int(5) NOT NULL,
  `_2_Stock` int(5) NOT NULL,
  `_1_Stock` int(5) NOT NULL,
  `Updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stock`
--

INSERT INTO `stock` (`id`, `Date`, `ShopID`, `_30_Stock`, `_20_Stock`, `_10_Stock`, `_5_Stock`, `_2_Stock`, `_1_Stock`, `Updated_at`) VALUES
(2, '2019-02-15', 2, 999, 999, 999, 999, 999, 999, '2019-02-15 10:39:15'),
(3, '2019-02-15', 3, 2000, 2000, 3000, 767, 767, 868, '2019-02-15 10:45:15');

-- --------------------------------------------------------

--
-- Table structure for table `stocklog`
--

CREATE TABLE IF NOT EXISTS `stocklog` (
`id` int(5) unsigned NOT NULL,
  `Date` date NOT NULL,
  `ShopID` int(5) NOT NULL,
  `_30_Stock` int(5) NOT NULL,
  `_20_Stock` int(5) NOT NULL,
  `_10_Stock` int(5) NOT NULL,
  `_5_Stock` int(5) NOT NULL,
  `_2_Stock` int(5) NOT NULL,
  `_1_Stock` int(5) NOT NULL,
  `Updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stocklog`
--

INSERT INTO `stocklog` (`id`, `Date`, `ShopID`, `_30_Stock`, `_20_Stock`, `_10_Stock`, `_5_Stock`, `_2_Stock`, `_1_Stock`, `Updated_at`) VALUES
(1, '2019-02-15', 3, 55, 454, 464, 767, 767, 868, '2019-02-15 09:58:50'),
(2, '2019-02-15', 2, 55, 1000, 464, 767, 767, 868, '2019-02-15 10:39:15'),
(3, '2019-02-15', 2, 55, 2000, 464, 767, 767, 868, '2019-02-15 10:44:54'),
(4, '2019-02-15', 3, 2000, 2000, 3000, 767, 767, 868, '2019-02-15 10:45:15'),
(5, '2019-02-15', 2, 999, 999, 999, 999, 999, 999, '2019-02-15 10:53:24');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(5) unsigned NOT NULL,
  `Fname` varchar(100) NOT NULL,
  `Lname` varchar(100) NOT NULL,
  `FatherName` varchar(100) NOT NULL,
  `DOB` varchar(10) NOT NULL,
  `Sex` varchar(10) NOT NULL,
  `Address` varchar(100) NOT NULL,
  `NID` varchar(20) NOT NULL,
  `SIS` varchar(50) NOT NULL,
  `Slot` varchar(100) NOT NULL,
  `IDCreationDate` varchar(10) NOT NULL,
  `Mobile` varchar(15) NOT NULL,
  `Email` varchar(100) NOT NULL,
  `Pass` varchar(100) NOT NULL,
  `Status` varchar(1) NOT NULL DEFAULT '1',
  `Type` varchar(20) NOT NULL DEFAULT 'Normal User',
  `Updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `Fname`, `Lname`, `FatherName`, `DOB`, `Sex`, `Address`, `NID`, `SIS`, `Slot`, `IDCreationDate`, `Mobile`, `Email`, `Pass`, `Status`, `Type`, `Updated_at`) VALUES
(1, 'Anup', 'Sarkar', '', '', '', '', '', '', '', '', '1676667712', 'admin@admin.com', '0e7517141fb53f21ee439b355b5a1d0a', '1', 'Super User', '2019-01-02 18:03:48'),
(2, 'test', 'account', '', '', '', '', '', '', '', '', '111111111', 'test@admin.com', '0e7517141fb53f21ee439b355b5a1d0a', '1', 'Super User', '2019-02-11 18:33:02');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `items`
--
ALTER TABLE `items`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop`
--
ALTER TABLE `shop`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stock`
--
ALTER TABLE `stock`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stocklog`
--
ALTER TABLE `stocklog`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
MODIFY `id` int(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `shop`
--
ALTER TABLE `shop`
MODIFY `id` int(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `stock`
--
ALTER TABLE `stock`
MODIFY `id` int(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `stocklog`
--
ALTER TABLE `stocklog`
MODIFY `id` int(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
