<?php

/**
 * @property  email
 */
class Stock_model extends CI_Model
{
    function __construct()
    {
        
        parent::__construct();
    }
    
    
    
    function insert($data,$date,$sid)
    {
        
        $stock =$this->getStock($date,$sid);
        
        $id = $stock[0]["id"];
        
        
        if($id==null)
        {
            
            
            if($this->db->insert('stock', $data))
            {
                if($this->insertLog($data))
                    return true;
                    else
                        return false;
                        
                        
            }
            else
                return false;
            
                
        }
        else
         {
            
            
             if( $this->update_stock($data, $id,$sid))
            {
                if($this->insertLog($data))
                    return true;
                    else
                        return false;
                        
                        
            }
            else
                return false;
            
                
         
         }
             
        
         
    }
    
  
    
    function insertLog($data)
    {
        return $this->db->insert('stocklog', $data);
    }
    
    
    public function update_stock($data,$id,$sid)
    {
        $this->load->database();
        
        $this->db->where('id', $id);
        $this->db->where('ShopID', $sid);
        
        $this->db->update('stock',$data);
        if($this->db->affected_rows() > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    
    public function get()
    {
     
        $this->db->select('s.id, s.Date,s.ShopID,s._30_Stock,s._20_Stock,s._10_Stock,s._5_Stock,s._2_Stock,s._1_Stock,shop.Shop_name')
        ->from('stock s')
        ->join('shop', 's.ShopID = shop.id');
  
        $query = $this->db->get();
        
        
        return $query->result();
    }
    
    
    public function getStockArray($id)
    {
        
        $this->db->select('s.id, s.Date,s.ShopID,s._30_Stock,s._20_Stock,s._10_Stock,s._5_Stock,s._2_Stock,s._1_Stock,shop.Shop_name')
        ->from('stock s')
        ->join('shop', 's.ShopID = shop.id')->where('s.id', $id);
        
        $query = $this->db->get();
        
        
        return $query->result_array();
    }
    
    
    
    
    public function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('stock');
        if($this->db->affected_rows() > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    
    
    public function getStock($date,$sid)
    {
        
        $this->db->select('*');
        $this->db->from('stock');
        
        $where = array('Date' => $date , 'ShopID' => $sid);
        
        $this->db->where($where);
        
        $sql= $this->db->get();
        
        
        $result=$sql->result_array();
        
        return $result;
    }
    
    
    
 
    
    
}