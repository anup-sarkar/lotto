<?php
defined('BASEPATH') or exit('No direct script access allowed');
$dat['flag'] = 'acc_c';
$this->load->view("module/admin_header", $dat);
?>


<div class="container" style="width: 50%; text-align: center;">
	<div class="card card-container" style="padding: 5%">
		<!-- <img class="profile-img-card" src="//lh3.googleusercontent.com/-6V8xOA6M7BA/AAAAAAAAAAI/AAAAAAAAAAA/rzlHcD0KYwo/photo.jpg?sz=120" alt="" /> -->
		<img id="profile-img" class="profile-img-card"
			style="width: 120px; margin: 0 auto"
			src="<?php echo base_url(); ?>assets/images/ticket.png" /> <br />
		<h2 id="profile-name" class="profile-name-card" style="color: #009df0">Update Stock</h2>
		<br />

  <?php

$attributes = array(
    "class" => "",
    "id" => "registerForm",
    "name" => "registerForm"
);
echo form_open("stock/create_stock", $attributes);


if($Stock != null)
{
    $id=$Stock[0]["id"];
    
    $shop=$Stock[0]["ShopID"];
    
    $date=$Stock[0]["Date"];
    
    $v30=$Stock[0]["_30_Stock"];
    
    $v20=$Stock[0]["_20_Stock"];
    $v10=$Stock[0]["_10_Stock"];
    $v5=$Stock[0]["_5_Stock"];
    $v2=$Stock[0]["_2_Stock"];
    $v1=$Stock[0]["_1_Stock"];
}

?>


											<div class="form-group">
													<h5>Shop</h5>
			<select class="form-control" name="ShopID">
                                                   
                                                       
                                                     <?php
                                                    foreach ($Shop as $item) {
                                                        
                                                        
                                                        if($Stock != null)                                                         
                                                        {
                                                            if($id==$item->id)
                                                            {
                                                                echo '<option selected value="' . $item->id . '">' . $item->Shop_name . ' </option>';
                                                                
                                                            }
                                                            else 
                                                            {
                                                                echo '<option value="' . $item->id . '">' . $item->Shop_name . ' </option>';
                                                                
                                                            }
                                                        }
                                                        else {
                                                            echo '<option value="' . $item->id . '">' . $item->Shop_name . ' </option>';
                                                             
                                                        }
                                                       
                                                    }
                                                    ?>
                                                  </select>
		</div>


		<div class="form-group">
				<h5>Date</h5>
			<input type="date" required id="Date" name="Date"
				class="form-control" value="<?php   if($Stock != null) echo $date; else echo set_value('Date'); ?>"> <span
				class="text-danger"><?php echo form_error('Date'); ?></span>
		</div>



 



		<div class="form-group">
		<h5>Stock Of $30</h5>
			<input type="number" required placeholder="Stock Of $30" id="v30"
				name="v30" class="form-control"
				value="<?php if($Stock != null) echo $v30; else echo set_value('v30'); ?>"> 
				
				<span class="text-danger"><?php  echo form_error('v30'); ?></span>
		</div>

	 
	 
	 <div class="form-group">
		<h5>Stock Of $20</h5>
			<input type="number" required placeholder="Stock Of $20" id="v20"
				name="v20" class="form-control"
				value="<?php if($Stock != null) echo $v20; else echo set_value('v20'); ?>"> 
				
				<span class="text-danger"><?php echo form_error('v20'); ?></span>
		</div>
	 

 
  <div class="form-group">
		<h5>Stock Of $10</h5>
			<input type="number" required placeholder="Stock Of $10" id="v10"
				name="v10" class="form-control"
				value="<?php if($Stock != null) echo $v10; else echo set_value('v10'); ?>"> 
				
				<span class="text-danger"><?php   echo form_error('v10'); ?></span>
		</div>
	 
 
			  
		  <div class="form-group">
		<h5>Stock Of $5</h5>
			<input type="number" required placeholder="Stock Of $5" id="v5"
				name="v5" class="form-control"
				value="<?php if($Stock != null) echo $v5; else echo set_value('v5'); ?>"> 
				
				<span class="text-danger"><?php   echo form_error('v5'); ?></span>
		</div>
	  <div class="form-group">
		<h5>Stock Of $2</h5>
			<input type="number" required placeholder="Stock Of $2" id="v20"
				name="v2" class="form-control"
				value="<?php if($Stock != null) echo $v2; else echo set_value('v2'); ?>"> 
				
				<span class="text-danger"><?php  echo form_error('v2'); ?></span>
		</div>
	 
	 
		  <div class="form-group">
		<h5>Stock Of $1</h5>
			<input type="number" required placeholder="Stock Of $1" id="v1"
				name="v1" class="form-control"
				value="<?php if($Stock != null) echo $v1; else echo set_value('v1'); ?>"> 
				
				<span class="text-danger"><?php echo form_error('v1'); ?></span>
		</div>
	 
		 
 
		<button class="btn btn-primary btn-block btn-signin" type="submit">
			<i class="fa fa-plus-circle" aria-hidden="true"></i> Update
		</button>
		
 
           
             <?php echo form_close(); ?>
                             
                             <br />
                             <?php echo $this->session->flashdata('msg'); ?>               
    </div>
	<!-- /card-container -->
</div>
<!-- /container -->





<br />

<style>

h5{text-align:left}
</style>
  
</body>
</html>

 